### Codes of Boundary-Assisted Learning for Building Extraction from Optical Remote Sensing Imagery

The codes are done with Python 3.6, based on TensorFlow 2.1.0, in Windows 10.
Please open the project with PyCharm or other IDE compatible with Python.