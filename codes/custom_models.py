import tensorflow.keras as keras
from custom_layers import L2
from custom_layers import ConvBnActLayer, DeConvBnActLayer
from custom_layers import SideExtractionLayer, SpatialVariance
from custom_layers import SeparableResLayer, ConvolutionAttentionLayer


class BALModel(keras.Model):
    def __init__(self, filters, rate=4, **kwargs):
        super(BALModel, self).__init__(**kwargs)
        # encoder
        self.conv = ConvBnActLayer(filters, 5, 1, 'same')
        self.conv_down1 = ConvBnActLayer(2*filters, 3, 2, 'same')
        self.res1 = keras.Sequential()
        for i in range(2):
            self.res1.add(SeparableResLayer(filters))
        self.conv_down2 = ConvBnActLayer(4*filters, 3, 2, 'same')
        self.res2 = keras.Sequential()
        for i in range(3):
            self.res2.add(SeparableResLayer(2*filters))
        self.conv_down3 = ConvBnActLayer(8*filters, 3, 2, 'same')
        self.res3 = keras.Sequential()
        for i in range(5):
            self.res3.add(SeparableResLayer(4*filters))
        self.conv_down4 = ConvBnActLayer(16*filters, 3, 2, 'same')
        self.res4 = keras.Sequential()
        for i in range(6):
            self.res4.add(SeparableResLayer(8*filters))
        # decoder
        self.up8 = SideExtractionLayer(16, 8)
        self.up4 = SideExtractionLayer(8, 4)
        self.up2 = SideExtractionLayer(4, 2)
        self.conv_up1 = DeConvBnActLayer(8*filters, 3, 2, 'same')
        self.concat1 = keras.layers.Concatenate()
        self.attention1 = ConvolutionAttentionLayer(16*filters, rate)
        self.conv1 = ConvBnActLayer(8*filters, 3, 1, 'same')
        self.conv_up2 = DeConvBnActLayer(4*filters, 3, 2, 'same')
        self.concat2 = keras.layers.Concatenate()
        self.attention2 = ConvolutionAttentionLayer(8*filters, rate)
        self.conv2 = ConvBnActLayer(4*filters, 3, 1, 'same')
        self.conv_up3 = DeConvBnActLayer(2*filters, 3, 2, 'same')
        self.concat3 = keras.layers.Concatenate()
        self.attention3 = ConvolutionAttentionLayer(4*filters, rate)
        self.conv3 = ConvBnActLayer(2*filters, 3, 1, 'same')
        self.conv4 = ConvBnActLayer(filters, 3, 1, 'same')
        self.conv5 = keras.layers.Conv2D(1, 3, 1, 'same', activation='sigmoid', kernel_regularizer=keras.regularizers.l2(L2))
        self.mask = keras.layers.UpSampling2D(interpolation='bilinear')
        self.variance = SpatialVariance()
        self.side_concat = keras.layers.Concatenate()
        self.boundary = keras.layers.Conv2D(1, 3, 1, 'same', activation='sigmoid', kernel_regularizer=keras.regularizers.l2(L2))

    def call(self, inputs, training=None, **kwargs):
        # encoder
        x = self.conv(inputs, training=training)
        x1 = self.conv_down1(x, training=training)
        x1 = self.res1(x1, training=training)
        x2 = self.conv_down2(x1, training=training)
        x2 = self.res2(x2, training=training)
        x3 = self.conv_down3(x2, training=training)
        x3 = self.res3(x3, training=training)
        x4 = self.conv_down4(x3, training=training)
        x4 = self.res4(x4, training=training)
        # decoder
        x = self.conv_up1(x4, training=training)
        x = self.concat1([x3, x])
        x = self.attention1(x)
        x = self.conv1(x, training=training)
        side3 = self.up8(x)
        x = self.conv_up2(x, training=training)
        x = self.concat2([x2, x])
        x = self.attention2(x)
        x = self.conv2(x, training=training)
        side2 = self.up4(x)
        x = self.conv_up3(x, training=training)
        x = self.concat3([x1, x])
        x = self.attention3(x)
        x = self.conv3(x, training=training)
        side1 = self.up2(x)
        x = self.conv4(x, training=training)
        x = self.conv5(x)
        mask = self.mask(x)
        variance = self.variance(mask)
        x = self.side_concat([variance, side1, side2, side3])
        boundary = self.boundary(x)
        # output
        return [mask, boundary]
