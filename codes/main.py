import cv2
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras as keras
from custom_models import BALModel


class BALNet:
    def __init__(self, shape, filters, rate):
        inputs = keras.Input(shape=shape, dtype=tf.float32)
        [mask, boundary] = BALModel(filters=filters, rate=rate)(inputs=inputs)
        self.model = keras.Model(inputs=inputs, outputs=[mask, boundary])
        self.model.summary()

    def predict(self, weights_path, image_path):
        self.model.load_weights(filepath=weights_path)
        image = cv2.imread(filename=image_path).astype('float32') / 255.0
        image = np.expand_dims(a=image, axis=0)
        prediction = self.model.predict(x=image)[0]
        prediction[prediction <= 0.5] = 0
        prediction[prediction > 0.5] = 1
        prediction = prediction.astype('uint8')
        plt.figure()
        plt.subplot(1, 2, 1)
        plt.axis('off')
        plt.title('image')
        plt.imshow(image[0, :, :, :])
        plt.subplot(1, 2, 2)
        plt.axis('off')
        plt.title('segmentation')
        plt.imshow(prediction[0, :, :, 0], cmap='gray')
        plt.show()


if __name__ == '__main__':
    # # example of Inria Aerial Dataset
    # net = BALNet(shape=(480, 480, 3), filters=36, rate=4)
    # weights_path = '../weights/weights_inria_aerial.h5'
    # image_path = '../examples/inria aerial images/inria_2.bmp'
    # net.predict(weights_path, image_path)

    # # example of WHU Aerial Building Dataset
    # net = BALNet(shape=(512, 512, 3), filters=36, rate=4)
    # weights_path = '../weights/weights_whu_aerial.h5'
    # image_path = '../examples/whu aerial images/whu_aeria_2.bmp'
    # net.predict(weights_path, image_path)

    # # example of WHU Satellite Building Dataset
    # net = BALNet(shape=(512, 512, 3), filters=36, rate=4)
    # weights_path = '../weights/weights_whu_satellite.h5'
    # image_path = '../examples/whu satellite images/whu_satellite_3.bmp'
    # net.predict(weights_path, image_path)

    pass
