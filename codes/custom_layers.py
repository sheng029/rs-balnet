import tensorflow as tf
import tensorflow.keras as keras

L2 = 1e-4
alpha = 0.02


class ConvBnActLayer(keras.layers.Layer):
    def __init__(self, filters, kernel_size, strides, padding, **kwargs):
        super(ConvBnActLayer, self).__init__(**kwargs)
        self.conv = keras.layers.Conv2D(filters, kernel_size, strides, padding,
                                        use_bias=False, kernel_initializer='he_uniform',
                                        kernel_regularizer=keras.regularizers.l2(L2))
        self.bn = keras.layers.BatchNormalization()
        self.act = keras.layers.LeakyReLU(alpha)

    def call(self, inputs, training=None, **kwargs):
        x = self.conv(inputs)
        x = self.bn(x, training=training)
        x = self.act(x)
        return x


class DeConvBnActLayer(keras.layers.Layer):
    def __init__(self, filters, kernel_size, strides, padding, **kwargs):
        super(DeConvBnActLayer, self).__init__(**kwargs)
        self.conv = keras.layers.Conv2DTranspose(filters, kernel_size, strides, padding,
                                                 use_bias=False, kernel_initializer='he_uniform',
                                                 kernel_regularizer=keras.regularizers.l2(L2))
        self.bn = keras.layers.BatchNormalization()
        self.act = keras.layers.LeakyReLU(alpha)

    def call(self, inputs, training=None, **kwargs):
        x = self.conv(inputs)
        x = self.bn(x, training=training)
        x = self.act(x)
        return x


class SeparableResLayer(keras.layers.Layer):
    def __init__(self, filters, **kwargs):
        super(SeparableResLayer, self).__init__(**kwargs)
        self.conv1x1 = keras.layers.Conv2D(filters, 1, 1, 'valid', use_bias=False,
                                           kernel_initializer='he_uniform',
                                           kernel_regularizer=keras.regularizers.l2(L2))
        self.bn1x1 = keras.layers.BatchNormalization()
        self.act1x1 = keras.layers.LeakyReLU(alpha)
        self.conv5x5 = keras.layers.SeparableConv2D(2*filters, 5, 1, 'same', use_bias=False,
                                                    depthwise_initializer='he_uniform',
                                                    pointwise_initializer='he_uniform',
                                                    depthwise_regularizer=keras.regularizers.l2(L2),
                                                    pointwise_regularizer=keras.regularizers.l2(L2))
        self.bn5x5 = keras.layers.BatchNormalization()
        self.act5x5 = keras.layers.LeakyReLU(alpha)
        self.add = keras.layers.Add()

    def call(self, inputs, training=None, **kwargs):
        x = self.conv1x1(inputs)
        x = self.bn1x1(x, training=training)
        x = self.act1x1(x)
        x = self.conv5x5(x)
        x = self.bn5x5(x, training=training)
        x = self.act5x5(x)
        x = self.add([x, inputs])
        return x


class ConvolutionAttentionLayer(keras.layers.Layer):
    def __init__(self, units, rate, **kwargs):
        super(ConvolutionAttentionLayer, self).__init__(**kwargs)
        self.max_pool = keras.layers.GlobalMaxPooling2D()
        self.ave_pool = keras.layers.GlobalAveragePooling2D()
        self.mlp = keras.Sequential()
        self.mlp.add(keras.layers.Conv2D(units // rate, 1, 1, 'valid',
                                         activation='relu',
                                         kernel_initializer='he_uniform',
                                         kernel_regularizer=keras.regularizers.l2(L2)))
        self.mlp.add(keras.layers.Conv2D(units, 1, 1, 'valid', kernel_regularizer=keras.regularizers.l2(L2)))
        self.add = keras.layers.Add()
        self.sigmoid = keras.layers.Activation('sigmoid')
        self.multiply1 = keras.layers.Multiply()

        self.concat = keras.layers.Concatenate()
        self.conv = keras.layers.Conv2D(1, 7, 1, 'same', activation='sigmoid',
                                        kernel_regularizer=keras.regularizers.l2(L2))
        self.multiply2 = keras.layers.Multiply()

    def call(self, inputs, **kwargs):
        shape = inputs.get_shape().as_list()
        v1 = self.max_pool(inputs)
        v2 = self.ave_pool(inputs)
        v1 = tf.expand_dims(v1, 1)
        v1 = tf.expand_dims(v1, 1)
        v2 = tf.expand_dims(v2, 1)
        v2 = tf.expand_dims(v2, 1)
        v1 = self.mlp(v1)
        v2 = self.mlp(v2)
        v = self.add([v1, v2])
        v = self.sigmoid(v)
        v = tf.tile(v, [1, shape[1], shape[2], 1])
        x = self.multiply1([v, inputs])

        s1 = tf.reduce_max(x, -1, True)
        s2 = tf.reduce_mean(x, -1, True)
        s = self.concat([s1, s2])
        s = self.conv(s)
        s = tf.tile(s, [1, 1, 1, shape[-1]])
        y = self.multiply2([s, x])
        return y


class SpatialVariance(keras.layers.Layer):
    def __init__(self, pool_size=3, **kwargs):
        super(SpatialVariance, self).__init__(**kwargs)
        self.pool_size = pool_size

    def call(self, inputs, **kwargs):
        return tf.math.abs(inputs - keras.layers.MaxPooling2D(self.pool_size, 1, 'same')(inputs))


class SideExtractionLayer(keras.layers.Layer):
    def __init__(self, kernel_size, strides, **kwargs):
        super(SideExtractionLayer, self).__init__(**kwargs)
        self.conv = keras.layers.Conv2D(1, 1, 1, 'valid', kernel_regularizer=keras.regularizers.l2(L2))
        self.up = keras.layers.Conv2DTranspose(1, kernel_size, strides, 'same', kernel_regularizer=keras.regularizers.l2(L2))

    def call(self, inputs, **kwargs):
        x = self.conv(inputs)
        x = self.up(x)
        return x
